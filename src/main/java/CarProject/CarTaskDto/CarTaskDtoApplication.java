package CarProject.CarTaskDto;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CarTaskDtoApplication {

	public static void main(String[] args) {
		SpringApplication.run(CarTaskDtoApplication.class, args);
	}

}
