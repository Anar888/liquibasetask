package CarProject.CarTaskDto.service;

import CarProject.CarTaskDto.dto.CarDto;
import CarProject.CarTaskDto.dto.CreateCarDto;
import CarProject.CarTaskDto.dto.UpdateCarDto;
import CarProject.CarTaskDto.model.Car;
import CarProject.CarTaskDto.repository.CarRepository;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
@RequiredArgsConstructor
public class CarService {
    public final CarRepository carRepository;
    public final ModelMapper modelMapper;

    public void create(CreateCarDto createCarDto) {
        Car car=modelMapper.map(createCarDto,Car.class);
        carRepository.save(car);
    }

    public void update(UpdateCarDto dto) {
        Optional<Car> entity= carRepository.findById(dto.getId());
        entity.ifPresent(car -> {

            car.setEngine(dto.getEngine());
            car.setColor(dto.getColor());
            car.setMaker(dto.getMaker());
            car.setModel(dto.getModel());
            carRepository.save(car);
        });

    }


    public CarDto get(Integer id) {
        Car car=carRepository.findById(id).get();
        CarDto carDto=modelMapper.map(car,CarDto.class);
        return carDto;

    }

    public void delete(Integer id) {

        carRepository.deleteById(id);
    }

    public List<CarDto> getAll() {
        List<Car> cars=carRepository.findAll();
        List<CarDto> carsdto= new ArrayList<>();

        cars.forEach(car ->
                carsdto.add(modelMapper.map(car, CarDto.class)));
        return carsdto;


    }
}
