package CarProject.CarTaskDto.dto;

import lombok.Data;

@Data
public class CreateCarDto {
    private String color;
    private Integer engine;
    private String model;
    private String maker;
}
